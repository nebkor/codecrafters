use std::{
    cell::RefCell,
    fmt::{Debug, Display},
};

use anyhow::Result as AnyResult;
use thiserror::Error;
use tokio::{
    io::{AsyncReadExt, AsyncWriteExt},
    net::{TcpListener, TcpStream},
};

const SEP: &str = "\r\n";

type PResult = Result<RedisVal, ParseError>;

trait EasyIter: Iterator {
    fn collect_vec(self) -> Vec<Self::Item>
    where
        Self: Sized,
    {
        self.collect()
    }
}

impl<T: Iterator + Sized> EasyIter for T {}

#[tokio::main]
async fn main() -> AnyResult<()> {
    let listener = TcpListener::bind("127.0.0.1:6379").await?;

    loop {
        let (mut stream, _) = listener.accept().await?;

        tokio::spawn(async move {
            handle_requests(&mut stream).await;
        });
    }
}

async fn handle_requests(stream: &mut TcpStream) {
    let (mut rx, mut tx) = stream.split();
    let mut buf = [0; 1024];

    let mut bites = rx.read(&mut buf).await.unwrap_or(0);
    while bites > 0 {
        let body = buf[0..bites].to_vec();
        let state = PState {
            pos: RefCell::new(0),
            buf: body,
        };
        let request = parse(&state).unwrap();
        let response = handle_command(&request);
        tx.write_all(&response).await.unwrap();
        tx.flush().await.unwrap();
        buf = [0; 1024];
        bites = rx.read(&mut buf).await.unwrap_or(0);
    }
    tx.flush().await.unwrap();
}

fn handle_command(request: &RedisVal) -> Vec<u8> {
    let mut args: Option<RedisVal> = None;
    let mut cmd = None;
    if let RedisVal::Array(req) = request {
        for (i, v) in req.iter().enumerate() {
            if i == 0 {
                if let RedisVal::String(s) = v {
                    cmd = Some(s.to_string());
                    args = Some(RedisVal::Array(req[1..].to_vec()));
                }
            }
        }
    }
    let cmd = cmd.map(|s| s.to_lowercase());
    match cmd {
        Some(cmd) => match cmd.as_ref() {
            "ping" => format!("+PONG{SEP}").as_bytes().to_vec(),
            "echo" => handle_echo(args.as_ref()),
            _ => panic!(),
        },
        None => panic!(),
    }
}

fn handle_echo(args: Option<&RedisVal>) -> Vec<u8> {
    let mut out = String::new();

    if let Some(args) = args {
        if let RedisVal::Array(args) = args {
            for arg in args {
                if let RedisVal::String(s) = arg {
                    let s = s.to_string();
                    let s = s.as_str();
                    out.push_str(&format!("${}{SEP}", s.len()));
                    out.push_str(s);
                    out.push_str(SEP);
                }
            }
        }
    }

    out.as_bytes().to_vec()
}

#[derive(Clone, Debug, PartialEq, Eq)]
enum RedisString {
    Simple(String),
    Bulk(Vec<u8>),
}

impl RedisString {
    pub fn to_string(&self) -> String {
        if let RedisString::Simple(s) = self {
            s.clone()
        } else if let RedisString::Bulk(v) = self {
            String::from_utf8(v.to_owned()).unwrap()
        } else {
            unreachable!()
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
enum RedisVal {
    String(RedisString),
    Array(Vec<RedisVal>),
}

#[derive(Debug, Error, PartialEq, Eq)]
enum ParseError {
    _BadEOF,
    Incomplete,
    Invalid,
    _InvalidAt(usize, u8),
}

impl Display for ParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{self:?}")
    }
}

#[derive(Default)]
struct PState {
    pos: RefCell<usize>,
    buf: Vec<u8>,
}

impl Debug for PState {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = String::from_utf8_lossy(&self.buf[self.pos.borrow().to_owned()..]);
        f.debug_struct("PState")
            .field("pos", &self.pos)
            .field("buf", &s)
            .finish()
    }
}

impl PState {
    pub fn get(&self) -> Option<&u8> {
        let idx = *self.pos.borrow();
        *self.pos.borrow_mut() += 1;
        self.buf.get(idx)
    }

    pub fn _peek(&self) -> Option<&u8> {
        self.buf.get(*self.pos.borrow() + 1)
    }

    pub fn take(&self, num: usize) -> Option<&[u8]> {
        let end = *self.pos.borrow() + num;
        if end <= self.buf.len() {
            let start = *self.pos.borrow();
            *self.pos.borrow_mut() = end;
            Some(&self.buf[start..end])
        } else {
            None
        }
    }

    pub fn get_while<F>(&self, pred: F) -> &[u8]
    where
        F: Fn(&u8) -> bool,
    {
        let start = *self.pos.borrow();
        // we want buf[start..end] to be valid, even if it's empty
        let mut end = start + 1;

        for (i, v) in self.buf[start..].iter().enumerate() {
            let cur = start + i;
            if pred(v) {
                // this is at least start + 1
                end = cur + 1;
            } else {
                break;
            }
        }
        *self.pos.borrow_mut() = end;
        &self.buf[start..end]
    }
}

fn parse(state: &PState) -> PResult {
    let g = state.get().unwrap();
    match g {
        b'+' => parse_simple_string(state),
        b'$' => parse_bulk_string(state),
        b'*' => parse_array(state),
        _ => unreachable!(),
    }
}

fn parse_simple_string(state: &PState) -> PResult {
    let s = state.get_while(|&c| c != b'\r');
    parse_exact(state, SEP.as_bytes())?;
    let s = String::from_utf8(s.to_vec()).map_err(|_| ParseError::Invalid)?;
    Ok(RedisVal::String(RedisString::Simple(s)))
}

fn parse_bulk_string(state: &PState) -> PResult {
    let len = parse_length(state)?;
    parse_exact(state, SEP.as_bytes())?;
    if let Some(s) = state.take(len) {
        Ok(RedisVal::String(RedisString::Bulk(s.to_vec())))
    } else {
        Err(ParseError::Incomplete)
    }
}

fn parse_array(state: &PState) -> PResult {
    let len = parse_length(state).unwrap();
    let mut out = Vec::with_capacity(len);

    for _ in 0..len {
        parse_exact(state, SEP.as_bytes())?;
        out.push(parse(state)?);
    }

    Ok(RedisVal::Array(out))
}

fn parse_length(state: &PState) -> Result<usize, ParseError> {
    let num = String::from_utf8(state.get_while(|n| n.is_ascii_digit()).to_vec())
        .map_err(|_| ParseError::Invalid)?;
    // this should definitely parse
    num.parse().map_err(|_| ParseError::Invalid)
}

fn parse_exact(state: &PState, pattern: &[u8]) -> Result<(), ParseError> {
    state
        .take(pattern.len())
        .and_then(|res: &[u8]| if res == pattern { Some(()) } else { None })
        .ok_or(ParseError::Invalid)
}
